using NUnit.Framework;
using Maior_dos_3;

namespace MaiorDos3.NUnitTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 2)]
        [TestCase(2, 3, 1)]
        [TestCase(2, 1, 3)]
        [TestCase(3, 1, 2)]
        [TestCase(3, 2, 1)]
        public void Maior_QuandoExecutadaComNumerosDiferentes_RetornaMaiorNumero(int a, int b, int c)
        {
            // arrange
            var descobrir = new Descobrir();
            descobrir.num1 = a;
            descobrir.num2 = b;
            descobrir.num3 = c;

            // act 
            var resultado = descobrir.ExecutarMaior();

            //assert
            Assert.That(resultado, Is.EqualTo(3));
        }

        [TestCase(1, 1, 2, 2)]
        [TestCase(1, 2, 1, 2)]
        [TestCase(2, 1, 1, 2)]
        public void Maior_QuandoExecutadaComDoisNumeroIguais_RetornaMaiorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var descobrir = new Descobrir();
            descobrir.num1 = a;
            descobrir.num2 = b;
            descobrir.num3 = c;

            // act 
            var resultado = descobrir.ExecutarMaior();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        [TestCase(1, 1, 1, 1)]
        public void Maior_QuandoExecutadaComTodosOsNumeroIguais_RetornaMaiorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var descobrir = new Descobrir();
            descobrir.num1 = a;
            descobrir.num2 = b;
            descobrir.num3 = c;

            // act 
            var resultado = descobrir.ExecutarMaior();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }
        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 2)]
        [TestCase(2, 3, 1)]
        [TestCase(2, 1, 3)]
        [TestCase(3, 1, 2)]
        [TestCase(3, 2, 1)]
        public void Maior_QuandoExecutadaComNumerosDiferentes_RetornaMenorNumero(int a, int b, int c,int esperado)
        {
            // arrange
            var descobrir = new Descobrir();
            descobrir.num1 = a;
            descobrir.num2 = b;
            descobrir.num3 = c;

            // act 
            var resultado = descobrir.ExecutarMenor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        [TestCase(1, 1, 2, 1)]
        [TestCase(1, 2, 2, 1)]
        [TestCase(2, 1, 1, 1)]
        public void Maior_QuandoExecutadaComDoisNumeroIguais_RetornaMenorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var descobrir = new Descobrir();
            descobrir.num1 = a;
            descobrir.num2 = b;
            descobrir.num3 = c;

            // act 
            var resultado = descobrir.ExecutarMenor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        [TestCase(1, 1, 1, 1)]
        public void Maior_QuandoExecutadaComTodosOsNumeroIguais_RetornaMenorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var descobrir = new Descobrir();
            descobrir.num1 = a;
            descobrir.num2 = b;
            descobrir.num3 = c;

            // act 
            var resultado = descobrir.ExecutarMenor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }
    }
}
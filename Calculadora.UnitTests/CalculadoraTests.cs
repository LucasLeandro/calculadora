using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTests
{
    public class CalculadoraTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Executar_QuandoAcaoForSomar_RetornaSoma()
        {

            // arrange
            var calculando = new Calculando();

            calculando.num = 1;
            calculando.num2 = 2;
            calculando.operacao = "+";

            // act
            var resultado = calculando.Executar();



            // assert
            Assert.That(resultado, Is.EqualTo(3));
        }

        [Test]
        public void Executar_QuandoAcaoForSubtrair_RetornaSubtracao()
        {

            // arrange
            var calculando = new Calculando();

            calculando.num = 1;
            calculando.num2 = 2;
            calculando.operacao = "-";

            // act
            var resultado = calculando.Executar();



            // assert
            Assert.That(resultado, Is.EqualTo(-1));
        }

        [Test]
        public void Executar_QuandoAcaoForDividir_RetornaDivisao()
        {

            // arrange
            var calculando = new Calculando();

            calculando.num = 1;
            calculando.num2 = 2;
            calculando.operacao = "/";

            // act
            var resultado = calculando.Executar();



            // assert
            Assert.That(resultado, Is.EqualTo(0.5));
        }
        [Test]
        public void Executar_QuandoAcaoForDividir_RetornaDivisaoNaN()
        {

            // arrange
            var calculando = new Calculando();

            calculando.num = 0;
            calculando.num2 = 0;
            calculando.operacao = "/";

            // act
            var resultado = calculando.Executar();



            // assert
            Assert.That(resultado, Is.NaN);
        }
        [Test]
        public void Executar_QuandoAcaoForDividirPorZero_RetornaDivisao()
        {

            // arrange
            var calculando = new Calculando();

            calculando.num = 1;
            calculando.num2 = 0;
            calculando.operacao = "/";

            // act
            var resultado = calculando.Executar();



            // assert
            Assert.That(resultado, Is.EqualTo(double.PositiveInfinity));
        }

        [Test]
        public void Executar_QuandoAcaoForMultiplicar_RetornaMultiplicacao()
        {

            // arrange
            var calculando = new Calculando();

            calculando.num = 1;
            calculando.num2 = 2;
            calculando.operacao = "*";

            // act
            var resultado = calculando.Executar();



            // assert
            Assert.That(resultado, Is.EqualTo(2));
        }
    }


}
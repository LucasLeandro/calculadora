﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMC
{
    class IMC
    {
      
       
        public double Calcular(Usuario usuario)
        {
            double imc = usuario.peso / (usuario.altura * usuario.altura);

            return imc;
        }

        public string Classificar(double imc)
        {
            if ( imc < 18.5)
            {
                return "Baixo Peso";
            }
            if (imc >=18.5 && imc <=24.9)
            {
                return "Peso Normal";
            }
            if (imc >=25 && imc <= 29.9)
            {
                return "Pré-Obeso";
            }
            if (imc >=30 && imc  <= 34.9)
            {
                return "Obeso I";
            }
            if (imc >35 && imc <= 39.9)
            {
                return "Obeso II";
            }
           
            
                return "Obeso III";
            



        }
    }
}

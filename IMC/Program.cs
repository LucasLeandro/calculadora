﻿using System;

namespace IMC
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario usuario = new Usuario();
            usuario.nome = "Lucas";
            usuario.peso = 60;
            usuario.altura = 1.77;
            usuario.idade = 20;
            usuario.email = "lucasassi@email.com";
            usuario.telefone = "99174373";


            IMC imc = new IMC();
             double valorimc = imc.Calcular(usuario);

            string classificacao = imc.Classificar(valorimc);
            
            Console.WriteLine(valorimc);
            Console.WriteLine(classificacao);
        }
    }
}
